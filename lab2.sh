#!/bin/bash

if [ $# -ne 0 ]; then
  echo "Використання: $0"
  exit 1
fi

DIR_PATH="/home/aleqpl/Documents"

if [ ! -d "$DIR_PATH" ]; then
  echo "Помилка: каталог $DIR_PATH не існує"
  exit 1
fi

ARCHIVE_NAME=$(basename "$DIR_PATH")
ARCHIVE_FILE="${ARCHIVE_NAME}.tar.gz"

tar -czf "$ARCHIVE_FILE" -C "$(dirname "$DIR_PATH")" "$ARCHIVE_NAME"

if [ $? -eq 0 ]; then
  echo "Створено стислий архів: $ARCHIVE_FILE"
else
  echo "Помилка при створенні архіву"
  exit 1
fi

DELETED_FILES=$(find "$DIR_PATH" -type f -mtime -1 -exec rm {} \; -print | wc -l)

echo "Видалено $DELETED_FILES файл(ів), створених сьогодні та 1 день тому"
